package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.api.model.permissions.OperationKey;
import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.BlueprintDialog;
import com.atlassian.confluence.webdriver.pageobjects.component.form.MultiUserPicker;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import it.com.atlassian.confluence.webdriver.pageobjects.FileListWizard;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.PAGE_RESTRICT;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;

/**
 * Tests the "File List" (formerly known as "Shared Files") Blueprint.
 */
public class FileListBlueprintTest extends AbstractBlueprintTest {
    static final String CREATE_DIALOG_MODULE_KEY = PLUGIN_KEY + ":file-list-item";
    static final String INDEX_PAGE_TITLE = "File lists";

    private String genericFileListPageTitle = "My files";

    @Test
    @ResetFixtures({"space"})
    public void newFileListPageAppearsOnIndexPage() throws Exception {
        FileListWizard wizard = openFileListWizard();

        String title = genericFileListPageTitle;
        final ViewPage fileListPage = wizard.setTitle(title).submit();
        assertThat(fileListPage.getTitle(), is(title));

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE, getExistingPage(fileListPage.getPageId()));
    }

    @Test
    @ResetFixtures({"space"})
    public void fileListWizardPreventsDuplicatePageTitle() throws Exception {
        FileListWizard wizard = openFileListWizard();

        wizard.setTitle(space.get().getHomepageRef().get().getTitle()).submitAndExpectWizardError();
        assertThat(wizard.getTitleError(), is("A page with this name already exists."));

        String uniqueTitle = genericFileListPageTitle;
        final ViewPage viewPage = wizard.setTitle(uniqueTitle).submit();
        assertThat(viewPage.getTitle(), is(uniqueTitle));
    }

    @Test
    @ResetFixtures({"space"})
    public void fileListWizardRequiresPageTitle() throws Exception {
        FileListWizard wizard = openFileListWizard();

        wizard.submitAndExpectWizardError();
        assertThat(wizard.getTitleError(), is("Name is required."));

        String title = genericFileListPageTitle;
        final ViewPage viewPage = wizard.setTitle(title).submit();
        assertThat(viewPage.getTitle(), is(title));
    }

    @Test
    @ResetFixtures({"space"})
    public void fileListWizardFieldsAreUsed() throws Exception {
        FileListWizard wizard = openFileListWizard();

        String title = "Here are some Shared Files";
        ViewPage viewPage = wizard
                .setTitle(title)
                .setDescription("These files are awesome.")
                .submit();

        assertThat(viewPage.getTitle(), is(title));
        assertThat(viewPage.getTextContent(), allOf(
                containsString("These files are awesome."),
                containsString("No files shared here yet")
        ));
    }

    // CONFDEV-15653
    @Test
    @ResetFixtures({"space"})
    public void anonymousUserWithCreateContentPermissionCanCreateFileListPage() {
        grantAnonymousPermissions();

        product.logOut();

        DashboardPage dashboardPage = product.visit(DashboardPage.class);
        assertFalse(dashboardPage.getHeader().isLoggedIn());

        String newTitle = "File list page by anonymous user";
        FileListWizard wizard = openFileListWizard();
        ViewPage viewPage = wizard.setTitle(newTitle).submit();
        assertThat(viewPage.getTitle(), is(newTitle));
    }

    @Test
    @ResetFixtures({"space"})
    public void filesAreRestrictedToSelectedUsers() {
        restClient.getAdminSession().permissions().removeSpacePermissions(space.get(), user.get(), PAGE_RESTRICT);

        FileListWizard wizard = openFileListWizard();

        String title = "Here are some Shared Files";
        ViewPage fileListPage = wizard
                .setTitle(title)
                .setDescription("These files are awesome.")
                .addUserRestriction(admin.get())
                .addUserRestriction(user.get())
                .submit();

        List<Map<String, String>> viewPermissionsSets = rpcClient.getAdminSession().getPermissionsComponent()
                .getContentPermissions(fileListPage.getPageId(), OperationKey.READ);
        List<String> viewPermissionsUsers = viewPermissionsSets.stream()
                .map(viewPermissionsSet -> viewPermissionsSet.get("userName"))
                .collect(Collectors.toList());

        assertThat(viewPermissionsUsers, hasSize(2));
        assertThat(viewPermissionsUsers, containsInAnyOrder(admin.get().getUsername(), user.get().getUsername()));
    }

    @Test
    @ResetFixtures({"space"})
    public void usernameIsEscapedProperly() {
        UserWithDetails userWithWeirdName = testUserFactory.createTestUser(
                testUserFactory.builder()
                        .username("\"><a>this is an error</a>")
                        .displayName("<strong>S</strong>cript Kiddy")
                        .email("evil@example.com"));
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        FileListWizard wizard = openFileListWizard();
        MultiUserPicker restrictedUserPicker = wizard
                .searchUserRestriction(userWithWeirdName)
                .selectUser(userWithWeirdName.getUsername());

        waitUntilTrue(restrictedUserPicker.containsUserValue(userWithWeirdName));
        assertThat(restrictedUserPicker.getDisplayedValues(), contains(userWithWeirdName.getFullName()));
    }

    private FileListWizard openFileListWizard() {
        BlueprintDialog blueprintDialog = openCreateDialog();
        blueprintDialog.openSpaceSelect().selectSpace(space.get());
        return selectBlueprint(blueprintDialog, FileListWizard.class, CREATE_DIALOG_MODULE_KEY);
    }
}