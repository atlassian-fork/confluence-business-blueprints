package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.model.content.SpaceType;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.webdriver.WebDriverConfiguration;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CommentsSection;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.plugins.whitelist.testing.WhitelistTestRule;
import it.com.atlassian.confluence.webdriver.pageobjects.SharelinksBookmarkletPage;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.COMMENT;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.PAGE_EDIT;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.plugins.whitelist.testing.WhitelistTestRule.withDefaultAdminLoginAndBaseUrl;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class SharelinksBookmarkletTest extends AbstractBlueprintTest {
    private static final String BASE_URL = WebDriverConfiguration.getBaseUrl();
    private static final String TEST_LOGIN_URL = BASE_URL + "/login.action";
    private static final String TEST_LOGIN_URL_TITLE = "Log In - Confluence";
    private static final String SHARELINKS_PAGE_LABEL = "shared-links";
    private static final String SPACE_TEST_NAME = "Bookmarklet Test";
    private static final String SPACE_TEST_KEY = "bookmarkletspacekey";
    private static final String TEST_NON_WHITELIST_URL = "http://test.nonwhitelisted.domain";


    @BeforeClass
    public static void ensureNoDefaultSpace() {
        Space defaultSpace = restClient.getAdminSession().spaceService().find().withKeys("ds").fetchOneOrNull();
        if (defaultSpace != null) {
            deleteSpaceAndWaitForResult(defaultSpace);
        }
    }

    @Override
    public void setUp() throws Exception {
        WhitelistTestRule whitelistTestRule = withDefaultAdminLoginAndBaseUrl(BASE_URL);
        whitelistTestRule.whitelistWildcard(BASE_URL);
        product.logOut();
        super.setUp();
    }

    @Override
    public void tearDown() {
        super.tearDown();

        product.getTester().getDriver().executeScript("localStorage && localStorage.clear();");
        Space personalSpace = restClient.getAdminSession().spaceService().find().withType(SpaceType.PERSONAL).fetchOneOrNull();
        if (personalSpace != null) {
            deleteSpaceAndWaitForResult(personalSpace);
        }
    }

    @Test
    public void testValidationRequired() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);
        bookmarkletPage.submit();

        assertThat("Incorrect URL field validation error", bookmarkletPage.getURLValidationError(), is("URL is required"));
        assertThat("Incorrect title field validation error", bookmarkletPage.getTitleValidationError(), is("Title is required"));
    }

    @Test
    public void testValidationForNonWhitelistURL() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);
        bookmarkletPage.setUrl(TEST_NON_WHITELIST_URL).setTitle("Not whitelist URL");
        bookmarkletPage.submit();

        assertThat("Incorrect wizard URL validation error", bookmarkletPage.getURLValidationError(), is("You cannot share a link to this site. Ask your admin to add it to the whitelist."));
    }

    @Test
    @ResetFixtures({"space"})
    public void testCreateDuplicatePageValidate() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);
        bookmarkletPage.setUrl(TEST_LOGIN_URL)
                .selectSpace(space.get().getName())
                .waitUntilTitleIsLoadedForUrl(TEST_LOGIN_URL_TITLE)
                .setTitle(space.get().getHomepageRef().get().getTitle())
                .submit();

        // Check duplicate validate error
        assertThat("Incorrect title validation error", bookmarkletPage.getTitleValidationError(), is("A page with this name already exists"));
    }

    @Test
    @ResetFixtures({"space"})
    public void testCreateSharelinksPage() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);
        waitUntilFalse(bookmarkletPage.isMessageShown());

        bookmarkletPage.setUrl(TEST_LOGIN_URL)
                .selectSpace(space.get().getName())
                .waitUntilTitleIsLoadedForUrl(TEST_LOGIN_URL_TITLE)
                .submit();

        ViewPage viewPage = product.viewPage(bookmarkletPage.getCreatedPageId());
        // check title of page
        assertThat("Incorrect page title", viewPage.getTitle(), is(TEST_LOGIN_URL_TITLE));
        // check sharelinks label of page
        assertThat("Expected page label not found", viewPage.getLabels(), hasItem(SHARELINKS_PAGE_LABEL));
    }

    @Test
    public void testMessageForAnonymousWithPermission() {
        grantAnonymousPermissions();
        product.logOut();

        SharelinksBookmarkletPage bookmarkletPage = product.visit(SharelinksBookmarkletPage.class);
        assertThat("Bookmarklet message link should contain login action", bookmarkletPage.getMessageLink(),
                containsString("login.action?os_destination="));
        waitUntilTrue("Bookmarklet page login form should be shown", bookmarkletPage.isFormShown());
    }

    @Test
    public void testMessageForAnonymousWithoutPermission() {
        restClient.getAdminSession().permissions().enableAnonymousUseConfluence();
        product.logOut();

        SharelinksBookmarkletPage bookmarkletPage = product.visit(SharelinksBookmarkletPage.class);
        assertThat("Bookmarklet message link should contain login action", bookmarkletPage.getMessageLink(),
                containsString("login.action?os_destination="));
        waitUntilFalse("Bookmarklet page form should not be shown", bookmarkletPage.isFormShown());
    }

    @Test
    public void testMessageForUserWithoutPermission() {
        restClient.getAdminSession().permissions().removeSpacePermissions(space.get(), user.get(), PAGE_EDIT);

        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);
        assertThat("Bookmarklet message link should contain administrator contact URL", bookmarkletPage.getMessageLink(),
                containsString("/confluence/wiki/contactadministrators.action"));
        waitUntilFalse("Bookmarklet page form should not be shown", bookmarkletPage.isFormShown());
    }

    @Test
    public void testGetLinkTitleUsingPasteUrl() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);

        bookmarkletPage.pasteUrl(TEST_LOGIN_URL);

        assertThat("Incorrect bookmarklet title", bookmarkletPage.getLoadedTitleForUrl(TEST_LOGIN_URL_TITLE), is(TEST_LOGIN_URL_TITLE));
    }

    @Test
    @ResetFixtures({"space"})
    public void testCommentPostedCorrectly() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);

        bookmarkletPage.setUrl(TEST_LOGIN_URL)
                .selectSpace(space.get().getName())
                .setComment("This is a comment")
                .waitUntilTitleIsLoadedForUrl(TEST_LOGIN_URL_TITLE)
                .submit();

        String createdPageId = bookmarkletPage.getCreatedPageId();
        ViewPage viewPage = product.viewPage(createdPageId);
        CommentsSection comments = viewPage.getComments();

        waitUntil("Expected a single comment", comments.timedSize(), is(1));
        waitUntil("Incorrect comment content", comments.get(0).getContent(), is("This is a comment"));
    }

    @Test
    public void testCannotAddCommentWithUserNoAddCommentPermission() {
        restClient.getAdminSession().permissions().removeSpacePermissions(space.get(), user.get(), COMMENT);

        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);

        bookmarkletPage.selectSpace(space.get().getName());
        bookmarkletPage.waitForDisabledCommentInput();
        waitUntilFalse("Bookmarklet page comment field should not be enabled", bookmarkletPage.isCommentFieldEnabled());
    }

    @Test
    @ResetFixtures({"space"})
    public void testLabelInPageAndLabelInTemplateContent() {
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);

        String labelName = "labeltest";
        bookmarkletPage.setUrl(TEST_LOGIN_URL)
                .selectSpace(space.get().getName())
                .addLabel(labelName)
                .waitUntilTitleIsLoadedForUrl(TEST_LOGIN_URL_TITLE)
                .submit();

        String createdPageId = bookmarkletPage.getCreatedPageId();
        ViewPage viewPage = product.viewPage(createdPageId);
        assertTrue("Page should jave label " + labelName, viewPage.getLabelSection().hasLabel(labelName));
    }

    @Test
    @ResetFixtures({"space"})
    public void testLastUsedSpaceIsSelectedByDefault() {
        rpcClient.getAdminSession().createPersonalSpace(user.get());
        SharelinksBookmarkletPage bookmarkletPage = product.login(user.get(), SharelinksBookmarkletPage.class);

        assertEquals("Incorrect space", "Personal space", bookmarkletPage.getSelectedSpace());

        bookmarkletPage.setUrl(TEST_LOGIN_URL)
                .selectSpace(space.get().getName())
                .waitUntilTitleIsLoadedForUrl(TEST_LOGIN_URL_TITLE)
                .submit();

        String createdPageId = bookmarkletPage.getCreatedPageId();
        product.viewPage(createdPageId);

        bookmarkletPage = product.visit(SharelinksBookmarkletPage.class);
        assertEquals("Incorrect space", space.get().getName(), bookmarkletPage.getSelectedSpace());
    }
}
