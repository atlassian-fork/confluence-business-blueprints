package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentStatus;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.BlueprintDialog;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.ListBlueprintTemplates;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.model.ItContentBlueprint;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.model.ItContentTemplateRef;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CreatePage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.plugin.ModuleCompleteKey;
import it.com.atlassian.confluence.webdriver.pageobjects.BlankIndexPage;
import it.com.atlassian.confluence.webdriver.pageobjects.DecisionsWizard;
import org.junit.After;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DecisionsBlueprintTest extends AbstractBlueprintTest {
    private static final String CREATE_DIALOG_MODULE_KEY = PLUGIN_KEY + ":decisions-blueprint-item";
    private static final String INDEX_PAGE_TITLE = "Decision log";

    private String genericDecisionsPageTitle = "My decisions";

    private CreatePage createPage;

    @After
    public void tearDown() {
        if (createPage != null) {
            createPage.getEditor().clickCancelAndWaitForPageReload();
            createPage = null;
        }
    }

    @Test
    @ResetFixtures({"space"})
    public void createDecisionGoesToEditPage() {
        DecisionsWizard wizard = openDecisionsWizard();
        String title = genericDecisionsPageTitle;

        createPage = wizard.setDecision(title).submit();
        assertThat(createPage.getTitle(), is(title));
    }

    @Test
    @ResetFixtures({"space"})
    public void createDecisionSaveEditorPageGoesToViewPage() {
        DecisionsWizard wizard = openDecisionsWizard();
        String title = genericDecisionsPageTitle;

        ViewPage viewPage = wizard.setDecision(title).submit().save();
        assertThat(viewPage.getTitle(), is(title));
    }

    @Test
    @ResetFixtures({"space"})
    public void newDecisionsPageAppearsOnIndexPage() {
        DecisionsWizard wizard = openDecisionsWizard();
        String title = genericDecisionsPageTitle;
        ViewPage decisionPage = wizard.setDecision(title).submit().save();

        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE, getExistingPage(decisionPage.getPageId()));
    }

    @Test
    @ResetFixtures({"space"})
    public void blankIndexPageHasStylesAndButton() {
        DecisionsWizard wizard = openDecisionsWizard();
        String title = genericDecisionsPageTitle;
        ViewPage decisionPage = wizard.setDecision(title).submit().save();
        Content decisionPageContent = getExistingPage(decisionPage.getPageId());

        // remove blueprint page to get blank index page
        restClient.getAdminSession().contentService().delete(decisionPageContent);
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE);

        BlankIndexPage blankIndexPage = product.getPageBinder().bind(BlankIndexPage.class);
        DecisionsWizard wizard2 = blankIndexPage.clickOnCreateButtonAndExpectWizard(DecisionsWizard.class);
        assertTrue(wizard2.isShown());
    }

    @Test
    @ResetFixtures({"space"})
    public void decisionsWizardPreventsDuplicatePageTitle() {
        DecisionsWizard wizard = openDecisionsWizard();

        wizard.setDecision(space.get().getHomepageRef().get().getTitle()).submitAndExpectWizardError();
        assertThat(wizard.getTitleError(), is("A page with this name already exists."));

        String title = genericDecisionsPageTitle;
        createPage = wizard.setDecision(title).submit();
        assertThat(createPage.getTitle(), is(title));
    }

    @Test
    @ResetFixtures({"space"})
    public void decisionsWizardRequiresPageTitle() {
        DecisionsWizard wizard = openDecisionsWizard();

        wizard.submitAndExpectWizardError();
        assertThat(wizard.getTitleError(), is("Decision is required."));

        String title = genericDecisionsPageTitle;
        createPage = wizard.setDecision(title).submit();
        assertThat(createPage.getTitle(), is(title));
    }

    @Test
    @ResetFixtures({"space"})
    public void anonymousUserWithCreateContentPermissionCanCreateDecisionsPage() {
        grantAnonymousPermissions();

        product.logOut();

        DashboardPage dashboardPage = product.visit(DashboardPage.class);
        assertFalse(dashboardPage.getHeader().isLoggedIn());

        DecisionsWizard wizard = openDecisionsWizard();
        String newTitle = "Decision page by anonymous user";
        createPage = wizard.setDecision(newTitle).submit();
        assertThat(createPage.getTitle(), is(newTitle));
    }

    @Test
    @ResetFixtures({"space"})
    public void outcomeFieldIsUsed() {
        DecisionsWizard wizard = openDecisionsWizard();

        String decisionPageTitle = genericDecisionsPageTitle;
        String decisionOutcome = "This is stupid.\n" +
                "This is a more stupid line";
        ViewPage decisionPage = wizard.setDecision(decisionPageTitle)
                .setStatus("Decided")
                .setOutcome(decisionOutcome)
                .submit()
                .save();

        assertThat(decisionPage.getTextContent(), containsString(decisionOutcome));
    }

    @Test
    public void outcomeIsToggledOnStatusChange() {
        DecisionsWizard wizard = openDecisionsWizard();
        assertFalse(wizard.isOutcomeVisible());

        wizard = wizard.setStatus("Decided");
        assertTrue(wizard.isOutcomeVisible());

        wizard = wizard.setStatus("Not started");
        assertFalse(wizard.isOutcomeVisible());
    }

    // CONFDEV-17222
    @Test
    @ResetFixtures({"space"})
    public void outcomesIsBlankIfStatusIsNotDecided() {
        DecisionsWizard wizard = openDecisionsWizard();
        wizard = wizard.setDecision("A Decision")
                .setStatus("Decided")
                .setOutcome("I have decided");

        ViewPage decisionPage = wizard.setStatus("Not started").submit().save();
        assertThat(decisionPage.getTextContent(), not(containsString("I have decided")));
    }

    @Test
    @ResetFixtures({"space"})
    public void backgroundFieldInsertedCorrectly() {
        DecisionsWizard wizard = openDecisionsWizard();
        String decisionBackground = "Anna is hungry and wants to buy lunch.\n" +
                "Alice is hungry too.";
        ViewPage decisionPage = wizard.setDecision("What to eat for lunch?")
                .setBackground(decisionBackground)
                .submit()
                .save();

        Content decisionPageContent = restClient.getAdminSession().contentService()
                .find(ExpansionsParser.parse(ContentService.DEFAULT_EXPANSIONS))
                .withId(ContentId.of(decisionPage.getPageId()))
                .fetchOneOrNull();
        assertThat("User input is not translated to html correctly",
                decisionPageContent.getBody().get(ContentRepresentation.STORAGE).getValue(),
                allOf(
                        containsString("Anna is hungry and wants to buy lunch."),
                        containsString("<br />Alice is hungry too.")));
    }

    @Test
    @ResetFixtures({"space"})
    public void editedIndexPageTemplateWorksAtSpaceScope() {
        ListBlueprintTemplates blueprintTemplates = product.login(admin.get(), ListBlueprintTemplates.class, space.get());

        ModuleCompleteKey blueprintKey = new ModuleCompleteKey(PLUGIN_KEY, "decisions-blueprint");
        ModuleCompleteKey templateKey = new ModuleCompleteKey(PLUGIN_KEY, "decisions-index-page");
        ItContentBlueprint blueprint = new ItContentBlueprint(blueprintKey.getCompleteKey());
        ItContentTemplateRef contentTemplate = new ItContentTemplateRef(templateKey.getCompleteKey(), blueprint);
        blueprintTemplates.edit(contentTemplate).save();

        DecisionsWizard wizard = openDecisionsWizard();

        String pageTitle = "Should we make the index page template editable?";
        ViewPage decisionPage = wizard.setDecision(pageTitle)
                .submit()
                .save();

        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE, getExistingPage(decisionPage.getPageId()));
    }

    // CONFDEV-24099
    @Test
    @ResetFixtures({"space"})
    public void decisionsPageHasDateLozenge() {
        DecisionsWizard wizard = openDecisionsWizard();
        String title = genericDecisionsPageTitle;
        String date = "2014-03-21";
        CreatePage editor = wizard.setDecision(title).setDate(date).submit();

        waitUntilTrue(editor.getEditor().getContent().htmlContains("</time>"));
        waitUntilTrue(editor.getEditor().getContent().htmlContains("datetime=\"" + date + "\""));
    }

    private DecisionsWizard openDecisionsWizard() {
        BlueprintDialog blueprintDialog = openCreateDialog();
        blueprintDialog.openSpaceSelect().selectSpace(space.get());
        return selectBlueprint(blueprintDialog, DecisionsWizard.class, CREATE_DIALOG_MODULE_KEY);
    }

    @Test
    @ResetFixtures({"space"})
    public void blueprintTemplateUsingCorrectMentionStorageFormat() {
        DecisionsWizard wizard = openDecisionsWizard();
        CreatePage editor = wizard
                .setDecision(genericDecisionsPageTitle)
                .setDate("2014-03-21")
                .addStakeholder(user.get().getUsername())
                .addStakeholder(admin.get().getUsername())
                .submit();

        Content decisionsDraft = restClient.getAdminSession().contentService()
                .find(ExpansionsParser.parse(ContentService.DEFAULT_EXPANSIONS))
                .withSpace(space.get())
                .withStatus(ContentStatus.DRAFT)
                .fetchOneOrNull();
        assertNotNull("decisionDraft is null", decisionsDraft);
        assertThat(decisionsDraft.getBody().get(ContentRepresentation.STORAGE).getValue(),
                not(containsString("<ri:user ri:username=")));
        assertThat(decisionsDraft.getBody().get(ContentRepresentation.STORAGE).getValue(),
                containsString("<ri:user ri:userkey="));
        editor.save();
    }
}
