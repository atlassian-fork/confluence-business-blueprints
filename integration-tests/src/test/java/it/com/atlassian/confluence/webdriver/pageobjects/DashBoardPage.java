package it.com.atlassian.confluence.webdriver.pageobjects;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

/**
 * Override original Dashboard to help both old and new version of confluence
 */
public class DashBoardPage extends com.atlassian.confluence.pageobjects.page.DashboardPage {

    @ElementBy(className = "aui-blanket")
    private PageElement auiBlanket;

    @ElementBy(id = "dashboard-onboarding-dialog")
    private PageElement onboardingDialog;

    @WaitUntil
    public void waitUntilPageIsReady() {
        if (this.onboardingDialog.timed().isPresent().byDefaultTimeout().booleanValue()) {
            PageElement skipOnboardingBtn = this.onboardingDialog.find(By.cssSelector(".aui-button.skip-onboarding"));
            if (skipOnboardingBtn.isPresent()) {
                skipOnboardingBtn.click();
            } else {
                // this is new onboarding dialog since 6.14
                this.onboardingDialog.find(By.cssSelector(".aui-button-primary.show-onboarding")).click();
            }

        }

        Poller.waitUntilFalse(this.onboardingDialog.timed().isPresent());
        Poller.waitUntilFalse(this.auiBlanket.timed().isVisible());
    }
}
