package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.model.people.Anonymous;
import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.test.usermanagement.TestUserFactory;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import it.com.atlassian.confluence.plugins.createcontent.webdriver.AbstractCreateContentTest;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ADMIN_PERMISSIONS;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.PAGE_EDIT;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.VIEW;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static org.junit.Assert.assertTrue;

@RunWith(ConfluenceStatelessTestRunner.class)
public class AbstractBlueprintTest extends AbstractCreateContentTest {
    protected static final String PLUGIN_KEY = "com.atlassian.confluence.plugins.confluence-business-blueprints";

    @Inject
    protected static TestUserFactory testUserFactory;

    @Fixture
    protected static UserFixture user = userFixture().build();
    @Fixture
    protected static UserFixture admin = userFixture().globalPermission(GlobalPermission.SYSTEM_ADMIN).build();
    @Fixture
    protected static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .permission(admin, ADMIN_PERMISSIONS)
            .build();

    @Before
    public void setUp() throws Exception {
        final DashboardPage dashboardPage = product.login(user.get(), DashboardPage.class);
        assertTrue("Should be logged in", dashboardPage.getHeader().isLoggedIn());
    }

    @After
    public void tearDown() {
        revokeAnonymousPermissions();
        restoreUserSpacePermissions();
    }

    protected static void deleteSpaceAndWaitForResult(Space space) {
        restClient.getAdminSession().longTask().doLongTaskAndWaitForResult(() ->
                restClient.getAdminSession().spaceService().delete(space), "Failed to delete space");
    }

    public void grantAnonymousPermissions() {
        restClient.getAdminSession().permissions().enableAnonymousUseConfluence();
        restClient.getAdminSession().permissions().addSpacePermissions(space.get(), new Anonymous(null, ""), VIEW, PAGE_EDIT);
    }

    public void revokeAnonymousPermissions() {
        restClient.getAdminSession().permissions().disableAnonymousUseConfluence();
        restClient.getAdminSession().permissions().removeSpacePermissions(space.get(), new Anonymous(null, ""), VIEW, PAGE_EDIT);
    }

    public void restoreUserSpacePermissions() {
        restClient.getAdminSession().permissions().addSpacePermissions(space.get(), user.get(), REGULAR_PERMISSIONS);
    }
}
