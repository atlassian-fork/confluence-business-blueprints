package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.test.mail.MailFacade;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.rules.WebSudoRule;
import com.atlassian.confluence.webdriver.WebDriverConfiguration;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.BlueprintDialog;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CommentsSection;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugins.whitelist.testing.WhitelistTestRule;
import it.com.atlassian.confluence.webdriver.pageobjects.BlankIndexPage;
import it.com.atlassian.confluence.webdriver.pageobjects.CustomHtmlPage;
import it.com.atlassian.confluence.webdriver.pageobjects.SharelinksWizard;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.mail.internet.MimeMessage;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.plugins.whitelist.testing.WhitelistTestRule.withDefaultAdminLoginAndBaseUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SharelinksBlueprintTest extends AbstractBlueprintTest {
    private static final String CREATE_DIALOG_MODULE_KEY = PLUGIN_KEY + ":sharelinks-blueprint-item";
    private static final String INDEX_PAGE_TITLE = "Shared links";

    private static final String IMAGE_META_TITLE = "Title image test";
    private static final String IMAGE_META_TITLE_TAG = "<meta property='og:title' content='" + IMAGE_META_TITLE + "' />";
    private static final String IMAGE_META_TAG = "<meta property='og:image' content='http://example.com/image-example.jpg' />";
    private static final String DESCRIPTION_META_TAG = "<meta property='og:description' content='Description test' />";
    private static final String VIDEO_META_TITLE_TAG = "<meta property='og:title' content='Title video test' />";
    private static final String VIDEO_META_TAG = "<meta property='og:video' content='http://youtube.com/watch?v=example' />";

    private static final String BASE_URL = WebDriverConfiguration.getBaseUrl();
    private static final String TEST_SPACE_DIR_URL = BASE_URL + "/spacedirectory/view.action";
    private static final String TEST_DASHBOARD_URL = BASE_URL + "/dashboard.action";
    private static final String TEST_NON_WHITELIST_URL = "http://test.nonwhitelisted.domain";
    private static final String CONTEXT_PATH = WebDriverConfiguration.getContextPath();

    private MailFacade mail;
    private boolean isCustomMetaTagsSet = false;

    @Rule
    public WebSudoRule webSudoRule = new WebSudoRule(false);

    @Override
    public void setUp() throws Exception {
        WhitelistTestRule whitelistTestRule = withDefaultAdminLoginAndBaseUrl(BASE_URL);
        whitelistTestRule.whitelistWildcard(BASE_URL);
        product.logOut();

        super.setUp();

        isCustomMetaTagsSet = false;
        mail = new MailFacade(rpcClient, restClient).start();
    }

    @Override
    public void tearDown() {
        super.tearDown();
        if (isCustomMetaTagsSet) {
            CustomHtmlPage customHtmlPage = product.login(admin.get(), CustomHtmlPage.class);
            customHtmlPage.clearCustomHtml().clickConfirmCustomHtml();
        }
        if (mail != null) {
            mail.stop();
        }
    }

    @Test
    public void testTitleFieldIsAutomaticallySet() {
        SharelinksWizard wizard = openShareLinksWizard();
        String url = BASE_URL + "/login.action";
        wizard = wizard.setUrl(url);

        String expectedTitle = "Log In - Confluence";
        assertThat("Incorrect wizard preview title", wizard.getPreviewTitle(), is(expectedTitle));
        assertThat("Incorrect wizard title", wizard.getTitle(), is(expectedTitle));
    }

    @Test
    public void testLinkPreviewIsRenderedInWizard() {
        SharelinksWizard wizard = openShareLinksWizard();
        String url = BASE_URL + "/login.action";
        wizard = wizard.setUrl(url);

        wizard.validateLinkPreviewRendered();
    }

    @Test
    public void testValidationRequired() {
        SharelinksWizard wizard = openShareLinksWizard();
        wizard.clickSubmit();

        assertThat("Incorrect wizard URL validation error", wizard.getURLValidationError(), is("URL is required"));
        assertThat("Incorrect wizard title validation error", wizard.getTitleValidationError(), is("Title is required"));
    }

    @Test
    public void testValidationForNonWhitelistURL() {
        SharelinksWizard wizard = openShareLinksWizard();
        wizard.setUrl(TEST_NON_WHITELIST_URL)
                .setTitle("Not whitelist URL")
                .clickSubmit();

        assertThat("Incorrect wizard URL validation error", wizard.getURLValidationError(), is("You cannot share a link to this site. Ask your admin to add it to the whitelist."));
    }

    @Test
    public void testBookmarkletIsPresent() {
        SharelinksWizard wizard = openShareLinksWizard();
        wizard.clickSubmit();

        wizard.validateBookmarkletPresent();
    }

    @Test
    @ResetFixtures({"space"})
    public void testCommentsPostedCorrectly() {
        SharelinksWizard wizard = openShareLinksWizard();

        String comment = "This is comment 1\n" +
                "This is comment 2";
        ViewPage viewPage = wizard.setUrl(TEST_SPACE_DIR_URL)
                .waitForPreviewLoaded()
                .setComment(comment)
                .submit();

        CommentsSection comments = viewPage.getComments();
        waitUntilEquals("Unexpected number of comments", 1, comments.timedSize());
        String comment1 = comments.get(0).getContent().byDefaultTimeout();
        assertThat("Incorrect first comment", comment, is(comment1));
    }

    @Test
    @ResetFixtures({"space"})
    public void testIndexHasPageAndHasButton() {
        SharelinksWizard wizard = openShareLinksWizard();

        String title = "Sharelinks1 title";
        ViewPage shareLinksPage = wizard.setUrl(TEST_DASHBOARD_URL)
                .setTitle(title)
                .submit();

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE, getExistingPage(shareLinksPage.getPageId()));
        ViewPage indexPage = product.getPageBinder().bind(ViewPage.class);

        SharelinksWizard wizard2 = indexPage.getCreateFromTemplateButton().clickAndExpectWizard(SharelinksWizard.class);
        wizard2.assertWizardIsShown();
    }

    @Test
    @ResetFixtures({"space"})
    public void testBlankIndexPageHasStylesAndButton() {
        SharelinksWizard wizard = openShareLinksWizard();

        String title = "Sharelinks1 title";
        ViewPage shareLinksPage = wizard.setUrl(TEST_DASHBOARD_URL)
                .waitForPreviewLoaded()
                .setTitle(title)
                .submit();
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        Content shareLinksPageContent = getExistingPage(shareLinksPage.getPageId());
        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE, shareLinksPageContent);

        // remove blueprint page to get blank index page
        restClient.getAdminSession().contentService().delete(shareLinksPageContent);
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        goToIndexPageAndCheckContent(space.get(), INDEX_PAGE_TITLE);
        BlankIndexPage blankIndexPage = product.getPageBinder().bind(BlankIndexPage.class);
        SharelinksWizard wizard2 = blankIndexPage.clickOnCreateButtonAndExpectWizard(SharelinksWizard.class);
        wizard2.assertWizardIsShown();
    }

    @Test
    @ResetFixtures({"space"})
    public void testPageIsAChildOfTheIndexPage() {
        SharelinksWizard wizard = openShareLinksWizard();

        ViewPage shareLinksPage = wizard
                .setUrl(TEST_DASHBOARD_URL)
                .setTitle("My Title")
                .submit();

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        // CONFSRVDEV-9790: Content#getParent() currently returns the furthest ancestor rather than the parent
        // TODO: use #assertParentPage() once CONFSRVDEV-9790 is fixed
        List<Content> shareLinksPageAncestors = getExistingPage(shareLinksPage.getPageId()).getAncestors();
        Content shareLinksIndexPage = getExistingPage(space.get(), INDEX_PAGE_TITLE);
        assertThat("Share links page parent should be the index page",
                shareLinksPageAncestors.get(shareLinksPageAncestors.size() - 1),
                is(shareLinksIndexPage));
    }

    @Test
    @ResetFixtures({"space"})
    public void testPageTitleValidation() {
        SharelinksWizard wizard = openShareLinksWizard();
        wizard.setUrl(TEST_DASHBOARD_URL)
                .setTitle(space.get().getHomepageRef().get().getTitle()) // use an existing name, expect error message
                .clickSubmit();
        assertThat("Incorrect wizard title validation error", wizard.getTitleValidationError(), is("A page with this name already exists"));

        // change the title, should now be able to create the page
        String title = "Sharelinks title";
        ViewPage viewPage = wizard.setTitle(title).submit();
        assertThat("Incorrect page title", viewPage.getTitle(), is(title));
    }

    @Test
    public void testPreviewLinkMetaTitleNotEmpty() {
        updateConfluenceMetaTags(IMAGE_META_TITLE_TAG, IMAGE_META_TAG, DESCRIPTION_META_TAG);
        SharelinksWizard wizard = openShareLinksWizard();
        wizard.setUrl(TEST_SPACE_DIR_URL);
        assertThat("Incorrect wizard preview title", wizard.getPreviewTitle(), is(IMAGE_META_TITLE));
    }

    @Test
    public void testPreviewLinkUsingPasteUrl() {
        updateConfluenceMetaTags(IMAGE_META_TITLE_TAG, IMAGE_META_TAG, DESCRIPTION_META_TAG);
        SharelinksWizard wizard = openShareLinksWizard();
        wizard.pasteUrl(TEST_SPACE_DIR_URL);
        assertThat("Incorrect wizard preview title", wizard.getPreviewTitle(), is(IMAGE_META_TITLE));
    }

    @Test
    @ResetFixtures({"space"})
    public void testDisplayLinkMetaDataInPageContent() {
        updateConfluenceMetaTags(IMAGE_META_TITLE_TAG, IMAGE_META_TAG, DESCRIPTION_META_TAG);
        SharelinksWizard wizard = openShareLinksWizard();

        String title = "Sharelinks display link meta data";
        ViewPage viewPage = wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle(title)
                .submit();

        waitUntil(viewPage.getMainContent().timed().getText(), allOf(containsString("Description test"), containsString("Open link")));

        final PageElement imageSelector = viewPage.getMainContent().find(By.cssSelector("div.sharelinks-link-meta-data h3 img"));
        waitUntilTrue("Image selector is not present", imageSelector.timed().isPresent());
    }

    @Test
    @ResetFixtures({"space"})
    public void testDisplayWhenNoMetaDataAvailable() {
        SharelinksWizard wizard = openShareLinksWizard();
        ViewPage viewPage = wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle("Sharelinks display link meta data")
                .submit();

        waitUntil(viewPage.getMainContent().timed().getText(), containsString("No link preview available. Please open the link for details."));
    }

    @Test
    @ResetFixtures({"space"})
    public void testDisplayWithImageAndNoDescription() {
        updateConfluenceMetaTags(IMAGE_META_TAG);
        SharelinksWizard wizard = openShareLinksWizard();
        ViewPage viewPage = wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle("Sharelinks display link meta data")
                .submit();

        waitUntil(viewPage.getMainContent().timed().getText(), containsString("No description available. Please open the link for details."));
        // page has meta data image
        final PageElement imageSelector = viewPage.getMainContent().find(By.cssSelector("div.sharelinks-link-meta-data h3 img"));
        waitUntilTrue("Image selector not present", imageSelector.timed().isPresent());
    }

    @Test
    @ResetFixtures({"space"})
    public void testDisplayVideoLinkMetaDataInPageContent() {
        updateConfluenceMetaTags(VIDEO_META_TITLE_TAG, IMAGE_META_TAG, VIDEO_META_TAG, DESCRIPTION_META_TAG);
        SharelinksWizard wizard = openShareLinksWizard();

        ViewPage viewPage = wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle("Sharelinks display video link meta data")
                .submit();

        waitUntil(viewPage.getMainContent().timed().getText(), allOf(containsString("Description test"), containsString("Open link")));
        final PageElement videoSelector = viewPage.getMainContent().find(By.cssSelector("img[src='http://example.com/image-example.jpg']"));
        waitUntilTrue(videoSelector.timed().isPresent());
    }

    @Test
    @ResetFixtures({"space"})
    public void anonymousUserWithoutCommentPermissionCannotComment() {
        grantAnonymousPermissions();

        product.logOut();

        DashboardPage dashboardPage = product.visit(DashboardPage.class);
        assertFalse("User should not be logged in", dashboardPage.getHeader().isLoggedIn());

        SharelinksWizard wizard = openShareLinksWizard();

        String newTitle = "Share links page by anonymous user";
        ViewPage viewPage = wizard.setUrl(WebDriverConfiguration.getBaseUrl() + "/spacedirectory/view.action")
                .setTitle(newTitle)
                .waitForCommentFieldToBeDisabled() // comment field is disabled
                .submit(); // user should be able to create the page

        assertThat("Incorrect page title", viewPage.getTitle(), is(newTitle));
        waitUntilTrue("Page comments section should be visible", viewPage.getComments().isVisible());
        waitUntilFalse("Top level comments area should not be visible", viewPage.getComments().isTopLevelCommentAreaVisible());
    }

    @Test
    @ResetFixtures({"space"})
    public void testLabelInPreviewAndLabelInTemplateContent() {
        SharelinksWizard wizard = openShareLinksWizard();

        wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle("Sharelinks label title")
                .addLabel("labeltest");

        ViewPage viewPage = wizard.submit();
        assertTrue("Page should have label " + "labeltest", viewPage.getLabelSection().hasLabel("labeltest"));
    }

    @Test
    public void testEscapesLabel() {
        SharelinksWizard wizard = openShareLinksWizard();

        wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle("Label XSS Test")
                .inputLabel("<u>ShouldNotBeUnderlined</u>");

        PageElement labelResultsField = wizard.getLabelPicker().getLabelResultsField();
        waitUntilFalse("Label error message is not escaped!", labelResultsField.find(By.cssSelector("u")).timed().isPresent());
    }

    @Test
    @ResetFixtures({"space"})
    public void testSharingLinkWithUser() throws Exception {
        SharelinksWizard wizard = openShareLinksWizard();

        String title = "Sharelinks label title";
        wizard.setUrl(TEST_SPACE_DIR_URL)
                .setTitle(title)
                .addUserToShareWith(user.get())
                .setComment("Share with you")
                .submit();

        MimeMessage smtpMessage = mail.waitForMessage();
        assertThat(smtpMessage.getSubject(), containsString(user.get().getDisplayName() + " shared \"" + title + "\" with you"));
    }

    @Test
    public void shouldNotAllowAccessUnauthorizedLinkForAnonymousUser() {
        try {
            restClient.getAdminSession().permissions().enableAnonymousUseConfluence();

            product.logOut();
            String url = BASE_URL + "/rest/sharelinks/1.0/link?url=" + TEST_NON_WHITELIST_URL;
            product.getTester().gotoUrl(url);
            assertThat(product.getTester().getDriver().getPageSource(), containsString("Not authorized to access " + TEST_NON_WHITELIST_URL + ". Please contact admin to whitelist it"));
        } finally {
            restClient.getAdminSession().permissions().disableAnonymousUseConfluence();
        }
    }

    @Test
    public void shouldNotAllowUnauthorizedUserAccess() {
        product.logOut();
        String url = BASE_URL + "/rest/sharelinks/1.0/link?url=" + TEST_NON_WHITELIST_URL;
        product.getTester().gotoUrl(url);
        assertThat(product.getTester().getDriver().getPageSource(), containsString("You are not authorized to access this resource"));
    }

    @Test
    public void shouldNotAllowAccessNonWhitelistShareLinkForUser() {
        product.getTester().gotoUrl(BASE_URL + "/rest/sharelinks/1.0/link?url=" + TEST_NON_WHITELIST_URL);
        assertThat(product.getTester().getDriver().getPageSource(),
                containsString("Not authorized to access " + TEST_NON_WHITELIST_URL + ". Please contact admin to whitelist it"));
    }

    private String getDomainName(final String baseUrl) {
        return baseUrl.replace(CONTEXT_PATH, "");
    }

    /**
     * Open the page Custom HTML in Confluence Admin, update meta tags follow open graph using Insert Custom HTML - At
     * end of the head html
     *
     * @param metaTags meta tags need input
     */
    private void updateConfluenceMetaTags(String... metaTags) {
        StringBuilder tags = new StringBuilder();

        for (String metaTag : metaTags) {
            tags.append(metaTag);
        }

        isCustomMetaTagsSet = true;
        CustomHtmlPage customHtmlPage = product.login(admin.get(), CustomHtmlPage.class);
        customHtmlPage.setCustomHeadHtml(tags.toString()).clickConfirmCustomHtml();
    }

    private SharelinksWizard openShareLinksWizard() {
        BlueprintDialog blueprintDialog = openCreateDialog();
        blueprintDialog.openSpaceSelect().selectSpace(space.get());

        return selectBlueprint(blueprintDialog, SharelinksWizard.class, CREATE_DIALOG_MODULE_KEY);
    }
}
