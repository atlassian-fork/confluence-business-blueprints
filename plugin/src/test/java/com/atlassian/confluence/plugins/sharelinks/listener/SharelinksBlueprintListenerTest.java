package com.atlassian.confluence.plugins.sharelinks.listener;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.sharepage.api.SharePageService;
import com.atlassian.confluence.plugins.sharepage.api.ShareRequest;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.user.UserKey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SharelinksBlueprintListenerTest {

    @Mock
    EventPublisher eventPublisher;
    @Mock
    CommentManager commentManager;
    @Mock
    SharePageService sharePageService;
    @Mock
    LabelManager labelManager;
    @Mock
    PermissionManager permissionManager;
    @Mock
    UserAccessor userAccessor;

    @Mock
    Page page;
    @Mock
    ConfluenceUser user;

    @InjectMocks
    SharelinksBlueprintListener sharelinksBlueprintListener;

    @Test
    public void testSharelinksWithInvalidUsers() {
        String passedInUserKeys = "userKeyOne,fakeUserKeyTwo,userKeyThree";
        String[] validUserKeys = new String[]{"userKeyOne", "userKeyThree"};

        when(userAccessor.getExistingUserByKey(new UserKey("userKeyOne"))).thenReturn(user);
        when(userAccessor.getExistingUserByKey(new UserKey("userKeyThree"))).thenReturn(user);

        sharelinksBlueprintListener.shareWithUsers(page, passedInUserKeys, "");

        ArgumentCaptor<ShareRequest> captor = ArgumentCaptor.forClass(ShareRequest.class);
        verify(sharePageService).share(captor.capture());

        ShareRequest shareRequest = captor.getValue();
        assertThat(shareRequest.getUsers(), containsInAnyOrder(validUserKeys));

    }
}
